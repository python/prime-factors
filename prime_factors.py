# -*- coding: utf-8 -*-

from math import*

def isPrime(n) :
    for i in range(2,floor(sqrt(n)+1)) :
        if n%i==0 : return False
    return True

def decompoPrime(n) :
    i=2
    r=n
    F=list()
    while r>1 :
        if r%i==0 :
            F.append(i)
            r//=i
        else :
            i+=1
            while not isPrime(i) :
                i+=1
    return F

def occurrences(value, lst):
    cnt = 0
    for elem in lst:
            if elem is value:
                    cnt += 1
    return cnt

def afficherFacteurs(lstF) :
    puiss=int()
    n=int()
    aff=""
    for i in range(0,len(lstF)) :
        n=lstF[i]
        if i>=1 :
            if not n==lstF[i-1] :
                puiss=occurrences(n,lstF)
                aff+=' x '+str(n)
                if puiss>1 : aff+='^'+str(puiss)
        else :
            puiss=occurrences(n,lstF)
            aff+=str(n)
            if puiss>1 : aff+='^'+str(puiss)

    return aff

while 1 :
    n=int(input("rentrer n : "))
    print("\n",n,"=",afficherFacteurs(decompoPrime(n)),"\n")

# -*- coding: utf-8 -*-

from math import*
import string

def isPrime(n) :
    for i in range(2,floor(sqrt(n)+1)) :
        if n%i==0 : return False
    return True

i = 0
c = list(string.ascii_lowercase)
n = 2
while i < 26 :
    if isPrime(n) :
        i += 1
        print(n,c[i-1])
    n += 1

# -*- coding: utf-8 -*-

from math import*

import matplotlib
import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
matplotlib.use('TkAgg')

from tkinter import *
from tkinter.ttk import *

def isPrime(n) :
    for i in range(2,floor(sqrt(n)+1)) :
        if n%i==0 : return False
    return True

def decompoPrime(n) :
    i=2
    r=n
    F=list()
    while r>1 :
        if r%i==0 :
            F.append(i)
            r//=i
        else :
            i+=1
            while not isPrime(i) :
                i+=1
    return F

def occurrences(value, lst):
    cnt = 0
    for elem in lst:
            if elem is value:
                    cnt += 1
    return cnt

def afficherFacteurs(lstF) :
    puiss=int()
    n=int()
    aff=""
    for i in range(0,len(lstF)) :
        n=lstF[i]
        if i>=1 :
            if not n==lstF[i-1] :
                puiss=occurrences(n,lstF)
                aff+='\\times'+str(n)
                if puiss>1 : aff+='^{'+str(puiss)+'}'
        else :
            puiss=occurrences(n,lstF)
            aff+=str(n)
            if puiss>1 : aff+='^{'+str(puiss)+'}'

    return aff

def graph(text):
    n=int(entry.get())
    tmptext = afficherFacteurs(decompoPrime(n))
    tmptext = "$"+tmptext+"$"

    ax.clear()
    ax.text(0.05, 0.5, tmptext, fontsize = 25-0.2*len(tmptext))  
    canvas.draw()


root = Tk()

mainframe = Frame(root)
mainframe.pack()

text = StringVar()
entry = Entry(mainframe, width=70, textvariable=text)
entry.pack()

label = Label(mainframe)
label.pack()

fig = matplotlib.figure.Figure(figsize=(5, 2), dpi=100)
ax = fig.add_subplot(111)

canvas = FigureCanvasTkAgg(fig, master=label)
canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=1)

ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

root.bind('<Return>', graph)
root.bind('<KP_Enter>', graph)
root.mainloop()
